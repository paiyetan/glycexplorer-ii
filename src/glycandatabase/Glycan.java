/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycandatabase;

import java.util.HashMap;

/**
 *
 * @author paiyeta1
 */
public class Glycan {

    
    private String glycan;
    private GlycanMass glycanMass;
    private String plusChargeCarrier;
    //private double mass;
    
    //Glycan(String glycan, HashMap<String,Double> glycanMonomerToMassMap){
    //    this.glycan = glycan;
    //    setMass(glycanMonomerToMassMap);
    //}
    
    Glycan(String glycan, HashMap<String,Double> glycanMonomerToMassMap, 
            double plusChargeMass,
            HashMap<String,String> config,
            String plusChargeCarrier){
        this.glycan = glycan;
        this.plusChargeCarrier = plusChargeCarrier;
        setMass(glycanMonomerToMassMap,plusChargeMass,config);
    }

    private void setMass(HashMap<String,Double> glycanMonomerToMassMap,
                            double plusChargeMass,
                                HashMap<String,String> config) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double mass = 0;
        //System.out.println(glycan);
        String[] monomersNCounts = glycan.split(";");
        //for(String monNC :monomersNCounts)
        //    System.out.print(monNC);
        //System.out.print("\n");
        
        for(int i = 0; i < monomersNCounts.length; i++){
            String monomerNCount = monomersNCounts[i];
            String[] monomerNCountArr = monomerNCount.split("-");
            String monomer = monomerNCountArr[0];
            int count = Integer.parseInt(monomerNCountArr[1]);
            //get monoisotopic mass of monomer unit...
            double monoisotopicMass = glycanMonomerToMassMap.get(monomer);
            double totalMonomerMass = monoisotopicMass * (double) count;
            
            /*
             * For every sialic acid molecule, add p-Toluidine's monoisotopic mass (less water molecule)
             * p-toluidine's monoisotopic mass is 107.0735 ( - 18.01055461) 
             */
            if(monomer.equalsIgnoreCase("S")){
                //compute added weight of p-toluidine
                //??? this assumes p-toluidine's modification of Sialic acids is complete...
                //get Sialic Acid modification...
                double modifxn = 0;
                String sialicAModifxn = config.get("Modifications");
                if(sialicAModifxn.equalsIgnoreCase("p-toluidine")){
                    modifxn = (107.0735 - 18.01055461) * (double) count;
                }//
                if(sialicAModifxn.equalsIgnoreCase("aniline")){
                    modifxn = (93.05785 - 18.01055461) * (double) count;
                }//
                totalMonomerMass = totalMonomerMass + modifxn;
            }
            
            //sum mass...
            mass = mass + totalMonomerMass;
        }
        // add free reducing end mass...
        mass = mass + 18.01055461;  
        //glycanMass = new GlycanMass(mass, (mass + 1.007825));
        glycanMass = new GlycanMass(mass, (mass + plusChargeMass));
        
    }
    
    @Override
    public boolean equals(Object glyc){
        boolean eq = false;
        Glycan glycToComp = (Glycan) glyc;
        if(glycToComp.getGlycan().equalsIgnoreCase(this.glycan)){
            eq = true;
        }
        return eq;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.glycan != null ? this.glycan.hashCode() : 0);
        return hash;
    }
    
    
    public String getGlycan() {
        return glycan;
    }

    public GlycanMass getMass() {
        return glycanMass;
    }
    
    public String getGlycanUnformattedString(){
        String unformatted = glycan.replaceAll(";", "").replaceAll("-", "") + this.plusChargeCarrier;
        return unformatted;
    }
    
      
    
    public class GlycanMass {
        private double absoluteMass;
        private double mHPlusMass;  
        
        GlycanMass(double absoluteMass, double mHPlusMass){
            this.absoluteMass = absoluteMass;
            this.mHPlusMass = mHPlusMass;
            
        }

        public double getAbsoluteMass() {
            return absoluteMass;
        }

        public double getMHPlusMass() {
            return mHPlusMass;
        }       
    }
 
}
