/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycandatabase;

import glycandatabase.enums.ModificationType;
import glycexplorer.parsers.LinearCode;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author paiyeta1
 */
public class GWBGlycan {
    
    private String linearCode;
    private double GivenMass;
    private GlycanComposition composition;
    private double computedMass;
    
    private int monomers; // number of momeric units (monomers/monosaccharides)...
    //private HashMap<String,Integer> monomerMap; // maps constituent/component monomers 
                                                  // to respective number of such in the glycan structure
    
    //private int Carbon;
    //private int Hydrogen;
    //private int Oxygen;
    //private int Nitrogen;
    //private int Phosphorus;   
    
    //private String Formula; // eg C6NH9O6P, C6H12O6
    private String glycanString; //stores glycan in the N4-F2-S2... format
    
    public GWBGlycan(String linearCode, double givenMass) {
        this.linearCode = linearCode;
        this.GivenMass = givenMass;
        
 
    }

    public double getModifiedMass(ModificationType modType){
        
        int noOfSailicAcids = 0;
        double modificationMass = 0; 
        double modifiedMass = 0;
        HashMap<String,Integer> glycMonomerTypeToNumberMap = 
                composition.getGlycMonomerTypeToNumberMap();
        HashMap<String,Integer> elementToNumberMap = 
                composition.getElementToNumberMap();
        
        switch(modType){
            /*
             * if(sialicAModifxn.equalsIgnoreCase("p-toluidine")){
                    modifxn = (107.0735 - 18.01055461) * (double) count;
                }//
                if(sialicAModifxn.equalsIgnoreCase("aniline")){
                    modifxn = (93.05785 - 18.01055461) * (double) count;
                }//
                
             */
            
            case PTOLUIDINE:
                // get number of Sialic acid...
                // multiply this by (107.0735 - 18.01055461)
                // add this to native mass +/-18.02
                if(glycMonomerTypeToNumberMap.containsKey("S")){
                    noOfSailicAcids = glycMonomerTypeToNumberMap.get("S");
                    modificationMass = noOfSailicAcids * (107.0735 - 18.01055461);
                }
                modifiedMass = this.GivenMass + modificationMass;               
                break;
                
            case ANILINE:
                // get number of Sialic acid...
                // multiply this by (93.05785 - 18.01055461)
                // add this to native mass +/-18.02
                if(glycMonomerTypeToNumberMap.containsKey("S")){
                    noOfSailicAcids = glycMonomerTypeToNumberMap.get("S");
                    modificationMass = noOfSailicAcids * (93.05785 - 18.01055461);
                }
                modifiedMass = this.GivenMass + modificationMass;               
                break;
                
            default: //PERMETHYLATED
                // NOTE: molecular wt. of CH3 is 15.03452 g/mol
                //  permethylation can occur on one to about 4 hydroxyl group per Hex, or HexNac monomer unit or
                    // one to abour 3/4 per Fucose monomer unit...
                
                //get total number of 'H' (Hexoses),'N' (N-Acetyl Hexoses), and 'F' (Fucoses)
                /*
                int h = 0; 
                int n = 0; 
                int f = 0;
                if(glycMonomerTypeToNumberMap.containsKey("H"))
                    h = glycMonomerTypeToNumberMap.get("H");
                if(glycMonomerTypeToNumberMap.containsKey("N"))
                    n = glycMonomerTypeToNumberMap.get("N");
                if(glycMonomerTypeToNumberMap.containsKey("F"))
                    f = glycMonomerTypeToNumberMap.get("F");
                
                modificationMass = ((h + n + f) * 15.03452);    
                modifiedMass = this.GivenMass + modificationMass; 
                * 
                */
                
                // get number of permethylable oxygen elements...
                //   permOxygens = TotalO2 - (glycosidics + ringO2 + sialylO2 + hexNAcO2)
                int permO2 = 0;
                int glycosidics = 0;
                int ringO2 = 0;
                int sialylO2 = 0;
                int hexNAcO2 = 0;
                // get number of monomeric units...
                monomers = 0;
                Set<String> comps = glycMonomerTypeToNumberMap.keySet();
                for(String comp : comps){
                    monomers = monomers + glycMonomerTypeToNumberMap.get(comp);
                }
                glycosidics = monomers - 1; // number of glycosidic bonds, 
                                        // and invariably sub-number of oxygens unavailable for permethylation
                ringO2 = monomers;
                if(glycMonomerTypeToNumberMap.containsKey("S"))
                    sialylO2 = glycMonomerTypeToNumberMap.get("S");
                if(glycMonomerTypeToNumberMap.containsKey("N"))
                    hexNAcO2 = glycMonomerTypeToNumberMap.get("N");
                
                permO2 = elementToNumberMap.get("O") - (glycosidics + ringO2 + sialylO2 + hexNAcO2);
                
                modificationMass = permO2 * 15.03452;    
                modifiedMass = this.GivenMass + modificationMass;  
                
                break;
          
        }
        return modifiedMass;        
    }
    
    public double getMassDiff(){
        return (computedMass - GivenMass); 
    }

    public String getLinearCode() {
        return linearCode;
    }

    public String getGlycanComposition() {
        return glycanString;
    }

    public double getNativeMass() {
        return GivenMass;
    }

    public void setComposition(GlycanComposition composition) {
        this.composition = composition;
        setGlycanString();
    }

    private void setGlycanString() {
        String str = "";
        HashMap<String,Integer> comp = composition.getGlycMonomerTypeToNumberMap();
        Set<String> keys = comp.keySet();
        for(String key : keys){
            int value = comp.get(key);
            str = str + key + value;
        }
        glycanString = str;
    }
    
    
    
}
