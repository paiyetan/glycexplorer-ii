/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycandatabase;

import glycexplorer.ios.Logger;
import glycexplorer.parsers.LinearCode;
import glycexplorer.parsers.LinearCodeParser;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class GWBGlycanDatabase {
    
    private LinkedList<GWBGlycan> glycans;
    
    
    public GWBGlycanDatabase(LinkedList<GWBGlycan> gwbGlycs, String xLCTablePath,
                                glycexplorer.ios.Logger logger) throws FileNotFoundException, IOException{
        glycans = gwbGlycs;
        parseLinearCodes(xLCTablePath, logger); // extract glycan monomers in 
        //computeMasses();       
    }

    public LinkedList<GWBGlycan> getGWBGlycans() {
        return glycans;
    }

    private void parseLinearCodes(String xLCTablePath, Logger logger) throws FileNotFoundException, IOException {
        // throw new UnsupportedOperationException("Not yet implemented");
        LinearCodeParser lCodeParser = new LinearCodeParser(xLCTablePath);
        logger.println("  Parsing glycan(s) linear code(s)....");
        // parse linear Code for rtespective glycans...
        for(GWBGlycan glycan : glycans){
            String linearCode  = glycan.getLinearCode();
            logger.println("    ..parsing: " + linearCode);
            GlycanComposition composition = lCodeParser.parseLinearCode(linearCode);
            glycan.setComposition(composition);
        }
        
    }

    /*
    private void computeMasses() {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    * 
    */
    
}
