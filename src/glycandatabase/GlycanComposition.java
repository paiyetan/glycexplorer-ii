/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycandatabase;

import java.util.HashMap;

/**
 *
 * @author paiyeta1
 */
public class GlycanComposition {
    
    private HashMap<String,Integer> glycMonomerTypeToNumberMap;
    private HashMap<String,Integer> elementToNumberMap;
    
    private double calculatedMass;

    public GlycanComposition(HashMap<String, Integer> glycMonomerTypeToNumberMap) {
        this.glycMonomerTypeToNumberMap = glycMonomerTypeToNumberMap;
    }
    
    public GlycanComposition(HashMap<String, Integer> glycMonomerTypeToNumberMap, HashMap<String, Integer> elementToNumberMap) {
        this.glycMonomerTypeToNumberMap = glycMonomerTypeToNumberMap;
        this.elementToNumberMap = elementToNumberMap;
    }

    public GlycanComposition(HashMap<String, Integer> glycMonomerTypeToNumberMap, HashMap<String, Integer> elementToNumberMap, double calculatedMass) {
        this.glycMonomerTypeToNumberMap = glycMonomerTypeToNumberMap;
        this.elementToNumberMap = elementToNumberMap;
        this.calculatedMass = calculatedMass;
    }

    public double getCalculatedMass() {
        return calculatedMass;
    }

    public HashMap<String, Integer> getElementToNumberMap() {
        return elementToNumberMap;
    }

    public HashMap<String, Integer> getGlycMonomerTypeToNumberMap() {
        return glycMonomerTypeToNumberMap;
    }
    
    
    
}
