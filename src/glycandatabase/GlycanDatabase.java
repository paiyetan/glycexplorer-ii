/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycandatabase;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author paiyeta1
 */
public class GlycanDatabase {
    
    
    private String[] monomers;
    private LinkedList<String> glycans;    
    private HashMap<String,Double> glycanMonomerToMassMap;
    private HashMap<String,Glycan> glycanToGlycanMap;
    private HashMap<Double,LinkedList<Glycan>> massToGlycansMap;
    

    
    public GlycanDatabase(String[] monomers, LinkedList<String> glycans,
                            glycexplorer.ios.Logger logger,
                            HashMap<String,String> config) {
        this.monomers = monomers;
        this.glycans = glycans; //unique unmodified glycans
        setGlycanMonomerToMassMap(logger);
        setGlycanToGlycanMap(logger,config);
        setMassToGlycansMap(logger);
    }

    
    public LinkedList<String> getGlycans() {
        return glycans;
    }

    
    public String[] getMonomers() {
        return monomers;
    }

    private void setGlycanMonomerToMassMap(glycexplorer.ios.Logger logger) {
        logger.println("  Mapping glycan monomers to respective mass...");
        glycanMonomerToMassMap = new HashMap<String,Double>();
        //char[] monomers = {'N', 'H', 'F', 'S'};
        //double[] monoistopicMasses = {
        glycanMonomerToMassMap.put("N", 203.0794 ); //HexNAc
        glycanMonomerToMassMap.put("H", 162.0528); //Hexose
        glycanMonomerToMassMap.put("F", 146.0579); //Fucose (DeoxyHex)
        glycanMonomerToMassMap.put("S", 291.0954); //NeuAc (Sialic Acid)
    }

    private void setGlycanToGlycanMap(glycexplorer.ios.Logger logger, HashMap<String,String> config) {
        //throw new UnsupportedOperationException("Not yet implemented");
        logger.println("  Mapping glycan (raw) string to associated glycan object...");
        glycanToGlycanMap = new HashMap<String,Glycan>();
        
        
        
        //Get the PlusChargeCarrier...
        String[] plusChargeCarriers = config.get("PlusChargeCarriers").split(","); // Na,H
        
        for(String glycan : glycans){
            for(String plusChargeCarrier : plusChargeCarriers){
                double plusChargeMass = 0; // defaults to H+
                if(plusChargeCarrier.equalsIgnoreCase("Na+")){//monoisotopic Mass 22.98977
                    plusChargeMass = 22.98977;
                    glycanToGlycanMap.put(glycan + "Na+", new Glycan(glycan, glycanMonomerToMassMap, 
                                                                    plusChargeMass, config, plusChargeCarrier));
                }
                if(plusChargeCarrier.equalsIgnoreCase("H+")){//monoisotopic Mass 22.98977
                    plusChargeMass = 1.007825;
                    glycanToGlycanMap.put(glycan + "H+", new Glycan(glycan, glycanMonomerToMassMap, 
                                                                    plusChargeMass, config, plusChargeCarrier));
                }
            }
            //glycanToGlycanMap.put(glycan, new Glycan(glycan, glycanMonomerToMassMap));
        }
    }

    private void setMassToGlycansMap(glycexplorer.ios.Logger logger) {
        //throw new UnsupportedOperationException("Not yet implemented");
        logger.println("  Mapping database estimated masses to glycan(s) ...");
        massToGlycansMap = new HashMap<Double,LinkedList<Glycan>>();
        Set<String> glycansMapped = glycanToGlycanMap.keySet();
        
        for(String glycan: glycansMapped){
            Glycan glycanObj = glycanToGlycanMap.get(glycan);
            Double mass = glycanObj.getMass().getMHPlusMass();
            if(massToGlycansMap.containsKey(mass)){
                LinkedList<Glycan> mappedGlycans = massToGlycansMap.remove(mass);
                mappedGlycans.add(glycanObj);
                massToGlycansMap.put(mass, mappedGlycans);//
            }else{
                LinkedList<Glycan> mappedGlycans = new LinkedList<Glycan>();
                mappedGlycans.add(glycanObj);
                massToGlycansMap.put(mass, mappedGlycans);//
            }
        }
    }
    
    public String getMassMappedGlycansConcatString(LinkedList<Glycan> glycansMapped){
        String formattedMappedGlycans = "NA";
        if(glycansMapped.size() > 0){
            for(int i = 0; i < glycansMapped.size(); i++){
                //Glycan glycan = new Glycan(glycansMapped.get(i), glycanMonomerToMassMap);
                String unformattedGlycan = glycansMapped.get(i).getGlycanUnformattedString();
                if(formattedMappedGlycans.equalsIgnoreCase("NA")){
                    formattedMappedGlycans = unformattedGlycan;
                    if((i+1) <= (glycansMapped.size() - 1)){
                        formattedMappedGlycans = formattedMappedGlycans + ";";
                    }
                }else{
                    formattedMappedGlycans = formattedMappedGlycans + unformattedGlycan;
                    if((i+1) <= (glycansMapped.size() - 1)){
                        formattedMappedGlycans = formattedMappedGlycans + ";";
                    }
                }

            }
        }        
        return formattedMappedGlycans;
    }
    
    public String getMassMappedGlycansDeltaMassesConcatString(LinkedList<Double> deltaMasses){
        String concatDeltaMasses = "NA";
        if(deltaMasses.size() > 0){
            for(int i = 0; i < deltaMasses.size(); i++){
                //Glycan glycan = new Glycan(glycansMapped.get(i), glycanMonomerToMassMap);
                String deltaMassString = String.valueOf(deltaMasses.get(i));
                if(concatDeltaMasses.equalsIgnoreCase("NA")){
                    concatDeltaMasses = deltaMassString;
                    if((i+1) <= (deltaMasses.size() - 1)){
                        concatDeltaMasses = concatDeltaMasses + ";";
                    }
                }else{
                    concatDeltaMasses = concatDeltaMasses + deltaMassString;
                    if((i+1) <= (deltaMasses.size() - 1)){
                        concatDeltaMasses = concatDeltaMasses + ";";
                    }
                }

            }
        }        
        return concatDeltaMasses;
    }
    
    
    
    

    public HashMap<String, Double> getGlycanMonomerToMassMap() {
        return glycanMonomerToMassMap;
    }

    public HashMap<String, Glycan> getGlycanToGlycanMap() {
        return glycanToGlycanMap;
    }

    public HashMap<Double, LinkedList<Glycan>> getMassToGlycansMap() {
        return massToGlycansMap;
    }
    
    
    
    
    
    
    
    
    
}
