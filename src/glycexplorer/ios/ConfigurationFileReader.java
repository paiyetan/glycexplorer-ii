/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.ios;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paiyeta1
 */
public class ConfigurationFileReader {
    
    /**
     * 
     * @param configFile 
     * @return
     */
    public HashMap<String,String> readConfig(String configFile){
        
        HashMap<String,String> config = new HashMap<String,String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(configFile));
            String line;
            while((line = reader.readLine())!=null){
                if(line.charAt(0)!='#'){
                    
                    String[] lineArr = line.split("#")[0].split("="); //allows in-line comments
                    String key = lineArr[0];
                    String value = lineArr[1];
                    config.put(key, value);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(ConfigurationFileReader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                Logger.getLogger(ConfigurationFileReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }       
        return config;
    }
    
}
