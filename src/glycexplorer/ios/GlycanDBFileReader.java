/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.ios;

import glycandatabase.GlycanDatabase;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author paiyeta1
 */
public class GlycanDBFileReader {    
    
    /**
     * 
     * @param dbFile
     * @param logger 
     * @param config 
     * @return
     */
    public GlycanDatabase read(String dbFile, glycexplorer.ios.Logger logger,
                                    HashMap<String,String> config){       
        BufferedReader reader = null;
        String[] monomers = null;
        LinkedList<String> glycans = new LinkedList<String>();
        String line;
        GlycanDatabase db = null;
        
        int lineCount = 0;
        try {
            reader = new BufferedReader(new FileReader(dbFile));
            while((line = reader.readLine())!=null){
                lineCount++;
                if(lineCount==1){ //header line
                    //get glycan monomers represented
                    monomers = line.split("\t");                   
                }else{
                    String[] lineArr = line.split("\t");
                    String glycan = "NA";
                    for(int i = 0; i < lineArr.length; i++){
                        //if(Integer.parseInt(lineArr[i])!= 0){
                            if(glycan.equalsIgnoreCase("NA")){
                                glycan = monomers[i] + "-" + lineArr[i];
                                if(i+1 <= lineArr.length-1){ //&&  Integer.parseInt(lineArr[i+1]) != 0){
                                    glycan = glycan + ";"; // add ";" in anticipation of next glycan structure...
                                }
                            }else{
                                glycan = glycan + monomers[i] + "-" + lineArr[i];
                                if( (i+1 <= lineArr.length-1)){ //&&  Integer.parseInt(lineArr[i+1]) != 0){
                                    glycan = glycan + ";"; // add ";" in anticipation of next glycan structure...
                                }
                            }
                        //}
                    } 
                    System.out.println((lineCount-1) + ". Glycan Found: " + glycan);
                    logger.println((lineCount-1) + ". Glycan Found: " + glycan);
                    glycans.add(glycan);
                }
            }            
           db  = new GlycanDatabase(monomers, glycans, logger, config);
            
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(GlycanDBFileReader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                Logger.getLogger(GlycanDBFileReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        
        return db;
    }
}
