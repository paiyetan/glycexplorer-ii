/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.ios;

import glycandatabase.GWBGlycan;
import glycandatabase.GWBGlycanDatabase;
import glycandatabase.enums.ModificationType;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class GlycoWBGlycanDBPrinter {

    public void print(GWBGlycanDatabase gwDb, String outFile) throws FileNotFoundException {
        //throw new UnsupportedOperationException("Not yet implemented");
        PrintWriter writer = new PrintWriter(outFile);
        // print Header
        writer.println("GlycanName(Linear Code)\t" + 
                            "Composition\t" + 
                            "NativeMass\t" + 
                                "ModifiedMass.PerMethylated\t" + 
                                "ModifiedMass.PToluidine\t" + 
                                "ModifiedMass.Aniline");
        // print body...
        LinkedList<GWBGlycan> gwbGlycans = gwDb.getGWBGlycans();
        for(GWBGlycan gwbGlycan : gwbGlycans){
            writer.println(gwbGlycan.getLinearCode() + "\t" + 
                            gwbGlycan.getGlycanComposition() + "\t" + 
                                toFiveDecimalPlaces(gwbGlycan.getNativeMass()) + "\t" + 
                                toFiveDecimalPlaces(gwbGlycan.getModifiedMass(ModificationType.PERMETHYLATED)) + "\t" + 
                                toFiveDecimalPlaces(gwbGlycan.getModifiedMass(ModificationType.PTOLUIDINE)) + "\t" + 
                            toFiveDecimalPlaces(gwbGlycan.getModifiedMass(ModificationType.ANILINE))
                           );
        }
        writer.close();
             
    }
    
    private String toFiveDecimalPlaces(double fig){
       String figure = String.valueOf(fig);
       String[] figArr = figure.split("\\.");
       String coef = figArr[0];
       if(figArr.length > 1){
            String mantissa = figArr[1];
            if(mantissa.length()==5){
                figure = coef + "." + mantissa;
            }else if(mantissa.length() < 5){
                int diff = 5 - mantissa.length();
                String trail = "";
                for(int i = 1; i <= diff; i++){
                    trail = trail + "0";
                }
                figure = coef + "." + mantissa + trail;
            }else{
                mantissa = mantissa.substring(0, 5);
                figure = coef + "." + mantissa;
            }
       }        
       return figure; 
    }
    
}



