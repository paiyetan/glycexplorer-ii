/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.ios;

import glycandatabase.GWBGlycan;
import java.io.*;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class GlycoWBenchSpectraFileReader {
    
    /*
     * public GWBGlycanDatabase read(String gwbSpectraFile){       
        GWBGlycanDatabase db = null;       
        return(db);
    }
    * 
    */
    
    public GWBGlycan readFile(File gwbSpectraFile) 
            throws FileNotFoundException, IOException{
        GWBGlycan glycan = null;
        BufferedReader reader = new BufferedReader(new FileReader(gwbSpectraFile));
        String line;
        // read first and second line
        line = reader.readLine(); //NAME:Ma3(Ma6)Mb4GNb4GN;N
        String linearCode = line.split(":")[1];
        linearCode = linearCode.split(";")[0];
        
        line = reader.readLine(); //Mass:910.816
        double givenMass = Double.parseDouble(line.split(":")[1]);
        
        reader.close();
        
        glycan = new GWBGlycan(linearCode, givenMass);
        return(glycan);       
    } 
    
    public LinkedList<GWBGlycan> readFilesInDir(String gwbSpectraFilesDir) throws FileNotFoundException, IOException{
        LinkedList<GWBGlycan> glycans = new LinkedList<GWBGlycan>();
        File[] gwbSpectFiles = new File(gwbSpectraFilesDir).listFiles();
        for(File gwbSpectFile:gwbSpectFiles){
            GWBGlycan glycan = readFile(gwbSpectFile);
            glycans.add(glycan);
        }        
        return(glycans);
    }
}
