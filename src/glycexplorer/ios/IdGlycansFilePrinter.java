/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.ios;

import glycandatabase.Glycan;
import glycandatabase.GlycanDatabase;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import glycexplorer.GlycExplorer;
import glycexplorer.GlycExplorerMappedGlycans;
import glycexplorer.msvalues.TandemMS;

/**
 *
 * @author paiyeta1
 */
public class IdGlycansFilePrinter {
    
    public void print(String mzMLFile, 
                            glycexplorer.ios.Logger logger, LinkedList<TandemMS> glycTandemMSes,
                            HashMap<Double, LinkedList<Glycan>> glycaDBMassToGlycansMap,
                            GlycanDatabase glycanDB,
                            HashMap<String,String> config){
        //printTandenMSes
        logger.println("Searching Glycan Database and Printing...");
        try {
            PrintWriter glycTandemMSPrinter = new PrintWriter(mzMLFile.replaceAll("mzML", "glyctandem"));
            //print table-header
            glycTandemMSPrinter.println("Scan Number\tPrecursor Mz\tPrecursor Charge\t" +
                                        "Retention time\tPeak Intensity\tMass(MH+/MNa+)\tMappedGlycans\tDeltaMasses");
            //print table-body
            for(TandemMS glycTandemMS : glycTandemMSes){
                //logger.println("  Searching DB for probable glycan(s) mapped to MS2 Spectra (Scan Number): " +
                //                    glycTandemMS.getScanNumber());
                
                double window = Double.parseDouble(config.get("deltaMassWindow"));
                GlycExplorerMappedGlycans mappedGlycans = glycTandemMS.getMappedGlycans(glycaDBMassToGlycansMap, window);
                //logger.println("    " + mappedGlycans.size() + " probable glycans mapped...");
                String mappedGlycansString = glycanDB.getMassMappedGlycansConcatString(mappedGlycans.getMappedGlycans());
                String mappedGlycansDeltaMaseseString = 
                        glycanDB.getMassMappedGlycansDeltaMassesConcatString(mappedGlycans.getDeltaMasses());
                
                glycTandemMSPrinter.println(glycTandemMS.getScanNumber() + "\t" +
                                            glycTandemMS.getPrecursorIonMz() + "\t" +
                                            glycTandemMS.getPrecursorIonCharge() + "\t" +
                                            glycTandemMS.getPrecursorIonRT() + "\t" +
                                            glycTandemMS.getPrecursorIonIntensity() + "\t" +
                                            glycTandemMS.getMonoIsotopicMHPlusMass() + "\t" +
                                            mappedGlycansString  + "\t" +
                                            mappedGlycansDeltaMaseseString);
                                            
            }
            glycTandemMSPrinter.close();              
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            java.util.logging.Logger.getLogger(GlycExplorer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
