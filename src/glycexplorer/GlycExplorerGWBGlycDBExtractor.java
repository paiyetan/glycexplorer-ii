/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer;

import glycandatabase.GWBGlycan;
import glycandatabase.GWBGlycanDatabase;
import glycexplorer.ios.ConfigurationFileReader;
import glycexplorer.ios.GlycoWBGlycanDBPrinter;
import glycexplorer.ios.GlycoWBenchSpectraFileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import utilities.OneDirChooser;

/**
 *
 * @author paiyeta1
 */
public class GlycExplorerGWBGlycDBExtractor {
    
    
    public static void main(String[] args) throws FileNotFoundException, IOException{
        OneDirChooser chooser;
        long start_time = new Date().getTime();
        //instantiate a loggerFile
        String outputId = String.valueOf(start_time);
        String logFile = "." + File.separator + "logs" + File.separator + outputId + ".msglycsearch.log";
        glycexplorer.ios.Logger logger = new glycexplorer.ios.Logger(logFile, true);
        logger.println("Starting...");
        
        chooser = new OneDirChooser("select an output folder...");
        String outDir = chooser.getOutputDir();
        //logger.println("Initiating Search...");
        logger.println("Loading configurations...");
        ConfigurationFileReader configReader = new ConfigurationFileReader();
        HashMap<String,String> config = configReader.readConfig("." + File.separator + "GlycExplorer-II.config");
        
        logger.println("Printing configurations...");
        logger.printConfiguration(config);
        
        //get Glycans from glycoWorkbench Files...
        logger.println("Loading GlycoWorkbench Glycans...");
        String gwbFilesDir = config.get("GlycoWorkbenchGlycanFilesDir"); //get GWB files
        String xlCTablePath = config.get("ExtendedLinearCodeLookupTablePath");
        
        GlycoWBenchSpectraFileReader gwbFileReader = new GlycoWBenchSpectraFileReader();
        LinkedList<GWBGlycan> gwbGlycans = gwbFileReader.readFilesInDir(gwbFilesDir);
        
        logger.println("Instantiating GlycoWorkbench-derived Glycans database...");
        GWBGlycanDatabase gwDb = new GWBGlycanDatabase(gwbGlycans, xlCTablePath,logger);
        
        //printDatabass
        logger.println("Printing database...");
        GlycoWBGlycanDBPrinter dBPrinter = new GlycoWBGlycanDBPrinter();
        String outFile = outDir + File.separator + "glycoWorkbenchDerivedGlycansDB.table";
        dBPrinter.print(gwDb,outFile);
        
        logger.println("...Done!!!");
        
        Date end_time = new Date();
        long end = end_time.getTime();
        // for logger...
        logger.println("End: " + end + ": " + end_time.toString());
        logger.println("Total time: " + (end - start_time) + " milliseconds; " + 
                        TimeUnit.MILLISECONDS.toMinutes(end - start_time) + " min(s), "
                        + (TimeUnit.MILLISECONDS.toSeconds(end - start_time) - 
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start_time))) + " seconds.");
        logger.close();       
    }    
}
