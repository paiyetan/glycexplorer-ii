/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer;

import glycandatabase.Glycan;
import glycandatabase.GlycanDatabase;
import glycexplorer.ios.ConfigurationFileReader;
import glycexplorer.ios.GlycanDBFileReader;
import glycexplorer.ios.IdGlycansFilePrinter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import glycexplorer.msvalues.FragmentIon;
import glycexplorer.msvalues.TandemMS;
import uk.ac.ebi.jmzml.model.mzml.BinaryDataArray;
import uk.ac.ebi.jmzml.model.mzml.CVParam;
import uk.ac.ebi.jmzml.model.mzml.PrecursorList;
import uk.ac.ebi.jmzml.model.mzml.Spectrum;
import uk.ac.ebi.jmzml.xml.io.MzMLObjectIterator;
import uk.ac.ebi.jmzml.xml.io.MzMLUnmarshaller;
import utilities.OneDirChooser;
import utilities.OneFileChooser;

/**
 *
 * @author paiyeta1
 */
public class GlycExplorer {
    
    public void searchMS(String mzMLFile, 
                            glycexplorer.ios.Logger logger, 
                                GlycanDatabase glycanDB,
                                    HashMap<String,String> config){        
        
        LinkedList<TandemMS> glycTandemMSes = new LinkedList<TandemMS>();
        // create a new unmarshaller object
        MzMLUnmarshaller unmarshaller = new MzMLUnmarshaller(new File(mzMLFile));
        
        logger.println("  Scanning mzML file for glycan associated spectra...");
        // unmarshall spectrum object
        //dealing with element collections
        MzMLObjectIterator<Spectrum> spectrumIterator = 
                unmarshaller.unmarshalCollectionFromXpath("/run/spectrumList/spectrum", Spectrum.class);
        while (spectrumIterator.hasNext()){
            //read next spectrum from XML file
            Spectrum spectrum = spectrumIterator.next();
            
            String spectrumID = spectrum.getId();
            String spectrumIDScanNumber = spectrumID.replace("controllerType=0 controllerNumber=1 scan=", "");
            
                    
            // BinaryDataArrayList bdArrayList = spectrum.getBinaryDataArrayList();
            // List<BinaryDataArray> bdArray = bdArrayList.getBinaryDataArray();
            // get only MS2 Level data
            String msLevel = "";
            List<CVParam> specParams = spectrum.getCvParam();
            Iterator<CVParam> itr = specParams.iterator();
            while(itr.hasNext()){
                CVParam msLevelParam = itr.next();
                if(msLevelParam.getAccession().equals("MS:1000511")){ //MS:1000511 is ms level param
                    msLevel = msLevelParam.getValue().trim();
                }
            }
            if(msLevel.equals("2")){ // interested in MS2 data only
                int scanNumber = Integer.parseInt(spectrumIDScanNumber);
                logger.println("    MS2 Signal scan number: " + scanNumber);
                Number[] mzNumbers = null;
                Number[] intenNumbers = null;
                //...read mz and intensity values...//
                List<BinaryDataArray> bdal = spectrum.getBinaryDataArrayList().getBinaryDataArray();
                for(BinaryDataArray bda: bdal){
                    List<CVParam> cvpList = bda.getCvParam();
                    for(CVParam cvp:cvpList){
                        if(cvp.getAccession().equals("MS:1000514")){
                            mzNumbers = bda.getBinaryDataAsNumberArray(); // fragment ion mzs
                        }
                        if(cvp.getAccession().equals("MS:1000515")){
                            intenNumbers = bda.getBinaryDataAsNumberArray(); // fragment ion intensities
                        }
                    }
                }
                
                // make fragment ion elements
                LinkedList<FragmentIon> fragmentIons = new LinkedList<FragmentIon>();
                for(int i = 0; i < mzNumbers.length; i++){
                    fragmentIons.add(new FragmentIon(intenNumbers[i].doubleValue(), mzNumbers[i].doubleValue()));
                }
                
                //if fragment ion fragmentMzes contain... any two of the following  fragmentMzes within a given ppm or Da
                // ******** insert verification code here ******* //
                logger.println("      scanning for glycan associated intensities....");
                if(hasGlycanAssociatedIntensities(fragmentIons, logger)){
                    // get scan number
                    // get precursor mass
                    // get precursor intensity
                    // get precursor charge 
                    // +/- get precursor retention time, precursorIonRT 
                    double precursorIonRT = 0;
                    double precursorIonMz = 0;
                    double peakIntensity = 0;
                    int precursorIonCharge = 0;
                    
                    
                    // precursor associated retention time...
                    //double rt = 0.0;
                    List<CVParam> scanParam = spectrum.getScanList().getScan().get(0).getCvParam();
                    for (Iterator lCVParamIterator = scanParam.iterator(); lCVParamIterator.hasNext();){
                        CVParam lCVParam = (CVParam) lCVParamIterator.next();
                        if (lCVParam.getAccession().equals("MS:1000016")){
                            precursorIonRT = Double.parseDouble(lCVParam.getValue().trim());
                        }
                    }
                    // other precursor-associated values...
                    PrecursorList plist = spectrum.getPrecursorList();
                    List<CVParam> scanPrecParam = 
                            plist.getPrecursor().get(0).getSelectedIonList().getSelectedIon().get(0).getCvParam();
                    
                    for (Iterator lCVParamIterator = scanPrecParam.iterator(); lCVParamIterator.hasNext();){
                        CVParam lCVParam = (CVParam) lCVParamIterator.next();
                        if (lCVParam.getAccession().equals("MS:1000744")){
                            precursorIonMz = Double.parseDouble(lCVParam.getValue().trim());
                        }
                        if (lCVParam.getAccession().equals("MS:1000041")){
                            precursorIonCharge = Integer.parseInt(lCVParam.getValue().trim());
                        }
                        if (lCVParam.getAccession().equals("MS:1000042")){
                            peakIntensity = Double.parseDouble(lCVParam.getValue().trim());
                        }
                    }                   
                    TandemMS foundGlycanMS = new TandemMS(scanNumber, precursorIonMz, precursorIonRT, 
                                                            precursorIonCharge, peakIntensity);
                    glycTandemMSes.add(foundGlycanMS);
                }
            }
        }        
        
        //map Glycan and GlycanMasses to MS2 mass estimates.... 
        HashMap<Double, LinkedList<Glycan>> glycaDBMassToGlycansMap = glycanDB.getMassToGlycansMap();
        IdGlycansFilePrinter printer = new IdGlycansFilePrinter();
        printer.print(mzMLFile, logger, 
                            glycTandemMSes, glycaDBMassToGlycansMap, 
                                    glycanDB, 
                                        config);
        
    }
    
    /*
     * private double extractReporterIntensity(double reporterMz, Number[] mzNumbers, Number[] intenNumbers) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double intensity = 0;
        for(int i = 0; i < mzNumbers.length; i++){
            if(reporterMz == Math.floor(mzNumbers[i].doubleValue() + 0.5)){
                intensity = intenNumbers[i].doubleValue();
                break;
            }
        }
        return intensity;
    }
    * 
    */
    
    private boolean hasGlycanAssociatedIntensities(LinkedList<FragmentIon> fragmentIons, glycexplorer.ios.Logger logger) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean has = false;
        logger.println("        associated Fragment Ion(s): " + fragmentIons.size());
        // get double values...
        //LinkedList<Double> fragmentMzes = new LinkedList<Double>();
        //for(Number mzNumber : mzNumbers){
        //    fragmentMzes.add(mzNumber.doubleValue());
        //}
        // sort values from least to maximum
        Collections.sort(fragmentIons);
        logger.println("          intensities; min: " + fragmentIons.get(0).getIntensity() +
                                                 ", max: " +  fragmentIons.get(fragmentIons.size() - 1).getIntensity() );
        // get top 10%
        LinkedList<FragmentIon> top10fragmentIons = new LinkedList<FragmentIon>();
        double factor = (100 - 10.00)/100;
        int top10PercentBeginIndex = ((int) (factor * fragmentIons.size()));
        for(int index = top10PercentBeginIndex ; index < fragmentIons.size(); index++){
            top10fragmentIons.add(fragmentIons.get(index));
        }
        logger.println("        top 10% Fragment Ion(s): " + top10fragmentIons.size());
        double[] standardFragmentMZesArray = {138.0549, 
                                                //168.0654, 
                                                //186.0759, 
                                                204.0865//, 
                                                //366.1386
                                             };
        //glycan fragment mzs ... 138.0549, 168.0654, 186.0759, 204.0865, 366.1386
        logger.println("          scanning top 10% Fragment Ion(s)...");
        //System.out.print("           found: ");
        LinkedList<Integer> foundAStandardFragmentMzes = new LinkedList<Integer>();
        for(int i = 0; i < standardFragmentMZesArray.length; i++){
            if(fragmentMZesContain(standardFragmentMZesArray[i], top10fragmentIons, logger)){
               foundAStandardFragmentMzes.add(1);
                //System.out.print(standardFragmentMZesArray[i] + "  ");
            }else{
                foundAStandardFragmentMzes.add(0);
            }
        }        
       // if(foundAStandardFragmentMzes.contains(1)) //if at least 1 standard fragment ion is present 
       //     has = true;
        has  = true; //assumes all oxonium ion were found
        for(int foundAStandardFragmentMz : foundAStandardFragmentMzes){
            if(foundAStandardFragmentMz==0){
                has = false;
                break;
            }
        }
              
        logger.print("\n");
        logger.println("            found glycan associated spectra: " + has);
        return has;
    }

    private boolean fragmentMZesContain(double standardFragmentMz, LinkedList<FragmentIon> top10fragmentIons,
            glycexplorer.ios.Logger logger) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean contain = false;
        for(FragmentIon top10fragmentIon : top10fragmentIons){
            double top10fragmentMz = top10fragmentIon.getMz();
            logger.println("            comparing " + standardFragmentMz + " to Fragment: " + top10fragmentIon.toString());
            if((top10fragmentMz >= (standardFragmentMz - 0.02)) && (top10fragmentMz <= (standardFragmentMz + 0.02))){
                
                contain = true;
                break;
            }
        }       
        return contain;
    }

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        OneFileChooser chooser;
        long start_time = new Date().getTime();
        //instantiate a loggerFile
        String outputId = String.valueOf(start_time);
        String logFile = "." + File.separator + "logs" + File.separator + outputId + ".msglycsearch.log";
        glycexplorer.ios.Logger logger = new glycexplorer.ios.Logger(logFile, true);
        logger.println("Starting...");
        
        //logger.println("Initiating Search...");
        logger.println("Loading configurations...");
        ConfigurationFileReader configReader = new ConfigurationFileReader();
        HashMap<String,String> config = configReader.readConfig("." + File.separator + "GlycExplorer-II.config");
        
        logger.printConfiguration(config);
        //readInGlycanDB....
        //chooser = new OneFileChooser("Select glycan database file...");
        //String glycanDBFile = chooser.getInputFile();
        String glycanDBFile = config.get("GlycanDatabase");
        
        //load glycan database...
        logger.println("Loading glycan database...");
        GlycanDBFileReader glycanDBFileReader = new GlycanDBFileReader();
        GlycanDatabase glycanDB = glycanDBFileReader.read(glycanDBFile, logger, config);
        
        logger.println("  " + glycanDB.getMonomers().length + " glycan \"monomeric units\" represented in database...");
        logger.println("  " + glycanDB.getGlycans().size() + " primary glycan \"structures\" represented in database...");
        logger.println("  " + glycanDB.getGlycanToGlycanMap().keySet().size() + " modified-glycan \"structures\" represented in database...");
        
        GlycExplorer explorer = new GlycExplorer();
        if (args.length > 0){
            //check what the flag is?
            if (args[0].equalsIgnoreCase("-d")){
                logger.println(" File(s) in directory search initiated...");
                OneDirChooser dirChooser = new OneDirChooser("Select mzML containing directory");
                String dir = dirChooser.getOutputDir();
                logger.println(" Input Directory: " + dir);
                //get mzML files
                File[] inputFiles = new File(dir).listFiles();
                LinkedList<String> mzMLFiles = new LinkedList<String>();
                for(File inputFile : inputFiles){
                    if(inputFile.getName().endsWith("mzML")){
                        mzMLFiles.add(inputFile.getAbsolutePath());
                    }
                }
                logger.println(" " + mzMLFiles.size() + " mzML file(s) found in directory...");
                //search each inputfile
                logger.println("  " + "searching...");
                int fileIndex = 0;
                for(String mzMLFile: mzMLFiles){
                    fileIndex++;
                    logger.println("  " + fileIndex + ": " + new File(mzMLFile).getName());
                    explorer.searchMS(mzMLFile, logger, glycanDB, config);
                }
            }
        }else{
            chooser = new OneFileChooser("Select mzML File...");
            String mzMLFile = chooser.getInputFile();
            explorer.searchMS(mzMLFile, logger, glycanDB, config);
        }
        
        //searcher.searchMS(mzMLFile, logger);
      
        
        logger.println("...Done!");
        
        Date end_time = new Date();
        long end = end_time.getTime();
        // for logger...
        logger.println("End: " + end + ": " + end_time.toString());
        logger.println("Total time: " + (end - start_time) + " milliseconds; " + 
                        TimeUnit.MILLISECONDS.toMinutes(end - start_time) + " min(s), "
                        + (TimeUnit.MILLISECONDS.toSeconds(end - start_time) - 
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start_time))) + " seconds.");
        logger.close();
    }

    
    
}
