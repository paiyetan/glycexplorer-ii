/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.msvalues;

/**
 *
 * @author paiyeta1
 */
public class FragmentIon implements Comparable{
    
    private double intensity;
    private double mz;

    public FragmentIon(double intensity, double mz) {
        this.intensity = intensity;
        this.mz = mz;
    }

    public double getIntensity() {
        return intensity;
    }

    public double getMz() {
        return mz;
    }

    @Override
    public int compareTo(Object o) {
        //throw new UnsupportedOperationException("Not supported yet.");
        int comparison = 0;
        FragmentIon fragmentToCompare = (FragmentIon) o;
        if(this.intensity < fragmentToCompare.getIntensity()){
            comparison = -1;
        } else if(this.intensity > fragmentToCompare.getIntensity()){
            comparison = 1;
        }       
        return comparison;
    }
    
    @Override
    public String toString(){
        return "m/z: " + this.mz + "; intensity: " + this.intensity;
    }
    
    
}
