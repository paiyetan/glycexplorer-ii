/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.msvalues;

import glycandatabase.Glycan;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import glycexplorer.GlycExplorerMappedGlycans;

/**
 *
 * @author paiyeta1
 */
public class TandemMS {
    
    // scan number
    // precursor mass
    // precursor intensity
    // precursor precursorIonCharge 
    // +/- retention time, precursorIonRT
    private int scanNumber;
    private double precursorIonMz;
    private double precursorIonRT;
    private int precursorIonCharge;
    private double precursorIonIntensity;
    
    private double[] fragmentIonIntensities;
    private double[] fragmentIonMzs;
    private double[] fragmentIonCharges;

    public TandemMS(int scanNumber, double precursorMass, int precursorCharge) {
        this.scanNumber = scanNumber;
        this.precursorIonMz = precursorMass;
        this.precursorIonCharge = precursorCharge;
    }

    public TandemMS(int scanNumber, double precursorMass, double precursorRT, int precursorCharge) {
        this.scanNumber = scanNumber;
        this.precursorIonMz = precursorMass;
        this.precursorIonRT = precursorRT;
        this.precursorIonCharge = precursorCharge;
    }
    
    public TandemMS(int scanNumber, double precursorMass, double precursorRT, int precursorCharge, 
                                            double precursorIonIntensity) {
        this.scanNumber = scanNumber;
        this.precursorIonMz = precursorMass;
        this.precursorIonRT = precursorRT;
        this.precursorIonCharge = precursorCharge;
        this.precursorIonIntensity = precursorIonIntensity;
    }

    public TandemMS(double precursorMz, double rT, int charge) {
        this.precursorIonMz = precursorMz;
        this.precursorIonRT = rT;
        this.precursorIonCharge = charge;
    }

    public TandemMS(double precursorMz, double rT, int charge, double[] fragmentIonIntensities) {
        this.precursorIonMz = precursorMz;
        this.precursorIonRT = rT;
        this.precursorIonCharge = charge;
        this.fragmentIonIntensities = fragmentIonIntensities;
    }

    public TandemMS(double precursorMz, double rT, int charge, double[] fragmentIonIntensities, double[] fragmentIonMzs) {
        this.precursorIonMz = precursorMz;
        this.precursorIonRT = rT;
        this.precursorIonCharge = charge;
        this.fragmentIonIntensities = fragmentIonIntensities;
        this.fragmentIonMzs = fragmentIonMzs;
    }

    public TandemMS(double precursorMz, double rT, int charge, double[] fragmentIonIntensities, double[] fragmentIonMzs, double[] fragmentIonCharges) {
        this.precursorIonMz = precursorMz;
        this.precursorIonRT = rT;
        this.precursorIonCharge = charge;
        this.fragmentIonIntensities = fragmentIonIntensities;
        this.fragmentIonMzs = fragmentIonMzs;
        this.fragmentIonCharges = fragmentIonCharges;
    }

    public int getPrecursorIonCharge() {
        return precursorIonCharge;
    }

    public double[] getFragmentIonCharges() {
        return fragmentIonCharges;
    }

    public double[] getFragmentIonIntensities() {
        return fragmentIonIntensities;
    }

    public double[] getFragmentIonMzs() {
        return fragmentIonMzs;
    }

    public double getPrecursorIonMz() {
        return precursorIonMz;
    }

    public double getPrecursorIonRT() {
        return precursorIonRT;
    }

    public int getScanNumber() {
        return scanNumber;
    }

    public double getPrecursorIonIntensity() {
        return precursorIonIntensity;
    }
    
    public double getMonoIsotopicMHPlusMass(){
        double mHplusMass = 0;
        //m/z = (M + zA)/z 
        // where M = mass, z = charges, A = mass of adduct providing charge; 
        // ref: http://www.enovatia.com/downloads/presentations/ProMass_training_slides.pdf
        
        // M = (mz * z) - zA
        mHplusMass = (this.precursorIonMz * this.precursorIonCharge) - 
                            (this.precursorIonCharge * 1.007825); // 1.007825 is the mass of H (the charge carrier)
        mHplusMass = mHplusMass + 1.007825; // MH+
        return mHplusMass;
    }
    
    public GlycExplorerMappedGlycans getMappedGlycans(HashMap<Double, LinkedList<Glycan>> massToGlycansMap, 
                                                double window){ //implementatain of a ppm unit window
         LinkedList<Glycan> mappedGlycans = new LinkedList<Glycan>();
         LinkedList<Double> deltaMasses = new LinkedList<Double>();
         
         //for each of the mapped masses in HashMap, get the mappedGlycans;
         Set<Double> masses = massToGlycansMap.keySet();
         for(double mass : masses){
             window = getAbsoluteWindow(mass, window);
             if((this.getMonoIsotopicMHPlusMass() >= (mass - window)) && 
                     (this.getMonoIsotopicMHPlusMass() <= (mass + window)) ){
                 LinkedList<Glycan> massMappedGlycans = massToGlycansMap.get(mass);
                 
                 
                 for(Glycan glyc : massMappedGlycans){
                     mappedGlycans.add(glyc);
                     double deltaMass = mass - (glyc.getMass().getMHPlusMass());
                     deltaMasses.add(deltaMass);
                 }
             }
         }
         
         
         
         return new GlycExplorerMappedGlycans(mappedGlycans,deltaMasses);
    }

    private double getAbsoluteWindow(double mass, double ppm) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double window = 0;
        // [[ppm/10^6]=[window/mass]]
        // window = [[ppm * mass]/10^6]
        window = ((ppm * mass)/1000000);       
        return window;
    }
    
    
    
     
    
}
