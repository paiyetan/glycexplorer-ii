/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer;

import glycandatabase.Glycan;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class GlycExplorerMappedGlycans {
    
    private LinkedList<Glycan> mappedGlycans;
    private LinkedList<Double> deltaMasses;

    public GlycExplorerMappedGlycans(LinkedList<Glycan> mappedGlycans, LinkedList<Double> deltaMasses) {
        this.mappedGlycans = mappedGlycans;
        this.deltaMasses = deltaMasses;
    }

    public LinkedList<Double> getDeltaMasses() {
        return deltaMasses;
    }

    public LinkedList<Glycan> getMappedGlycans() {
        return mappedGlycans;
    }

    
}
