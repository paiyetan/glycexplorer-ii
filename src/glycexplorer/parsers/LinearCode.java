/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.parsers;

/**
 *
 * @author paiyeta1
 */
public class LinearCode {
    
    private String Code;
    private String FullName;
    private String CommonConfig;
    //private String superClass;
    //private String superClassSymbol;
    //private double molecularMass
    private String Class;
    private String ClassSymbol;
    private double UnitMolarMass;
    private String Formula;
    private int Carbon;
    private int Hydrogen;
    private int Oxygen;
    private int Nitrogen;
    private int Phosphorus;   
    

    public LinearCode(String code, String fullName) {
        this.Code = code;
        this.FullName = fullName;
    }

    public LinearCode(String code, String fullName, 
                        String commonConfig) {
        this.Code = code;
        this.FullName = fullName;
        this.CommonConfig = commonConfig;
    }

    public LinearCode(String code, String fullName, 
                        String commonConfig, String Class) {
        this(code,fullName,commonConfig);
        this.Class = Class;
    }

    public LinearCode(String code, String fullName, 
                        String commonConfig, String Class, 
                                String ClassSymbol) {
        this(code,fullName,commonConfig,Class);
        this.ClassSymbol = ClassSymbol;
    }

    public LinearCode(String Code, String FullName, String CommonConfig, 
                        String Class, String ClassSymbol, double UnitMolarMass) {
        this(Code,FullName,CommonConfig,Class, ClassSymbol);
        this.UnitMolarMass = UnitMolarMass;
    }

    public LinearCode(String Code, String FullName, String CommonConfig, 
                        String Class, String ClassSymbol, double UnitMolarMass, String Formula) {
        this(Code,FullName,CommonConfig,Class, ClassSymbol, UnitMolarMass);
        this.Formula = Formula;
    }

    public LinearCode(String Code, String FullName, String CommonConfig, 
                        String Class, String ClassSymbol, double UnitMolarMass, 
                            String Formula, int Carbon, int Hydrogen, 
                                int Oxygen, int Nitrogen, int Phosphorus) {
        this(Code,FullName,CommonConfig,Class, ClassSymbol, UnitMolarMass,Formula);
        this.Carbon = Carbon;
        this.Hydrogen = Hydrogen;
        this.Oxygen = Oxygen;
        this.Nitrogen = Nitrogen;
        this.Phosphorus = Phosphorus;
    }  
    
    public String getCode() {
        return Code;
    }

    public String getCommonConfig() {
        return CommonConfig;
    }

    public String getFullName() {
        return FullName;
    }

    public String getLinearCodeClass() {
        return Class;
    }

    public String getClassSymbol() {
        return ClassSymbol;
    }

    public int getCarbon() { // number of carbon molecules in represented glycan monomer
        return Carbon;
    }

    public String getFormula() { // Chemical formula of represented glycan
        return Formula;
    }

    public int getHydrogen() { // number of Hydrogen molecules in represented glycan monomer
        return Hydrogen;
    }

    public int getNitrogen() { // number of Nitrogen molecules in represented glycan monomer
        return Nitrogen;
    }
    
    public int getOxygen() { // number of Oxygen molecules in represented glycan monomer
        return Oxygen;
    }

    public int getPhosphorus() { // number of Phosphorus molecules in represented glycan monomer
        return Phosphorus;
    }

    public double getUnitMolarMass() { 
        return UnitMolarMass;
    }
    
    public void removeH2O(){
        // remove water molecule
        Hydrogen = Hydrogen-2;
        Oxygen = Oxygen-1;
        UnitMolarMass = UnitMolarMass - 18.01528;
    }

    
    
}
