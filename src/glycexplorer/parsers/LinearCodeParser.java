/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package glycexplorer.parsers;

import glycandatabase.GlycanComposition;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class LinearCodeParser {
    
    //private LinkedList<LinearCode> lCode;
    private HashMap<String, LinearCode> codeToLinearCodeMap;
    
    public LinearCodeParser(String xLCTablePath) throws FileNotFoundException, IOException{
        //lCode = new LinkedList<LinearCode>();
        codeToLinearCodeMap = new HashMap<String, LinearCode>(); 
        
        BufferedReader reader = new BufferedReader(new FileReader(xLCTablePath));
        String line;
        int linesRead = 0;
        while((line = reader.readLine())!=null){
            linesRead++;
            if(linesRead > 1){
                String[] lineArr = line.split("\t");
                /*
                * LinearCode	
                * FullName	
                * CommonConfig	Class	ClassSymbol	
                * UnitMolecularMass(g/mol)	Formula	Carbon	Hydrogen	
                * Oxygen	Nitrogen	Phosphorous

                */

                String Code = lineArr[0];
                String FullName = lineArr[1];
                String CommonConfig = lineArr[2];
                String Class = lineArr[3];
                String ClassSymbol = lineArr[4];
                double UnitMolarMass = Double.parseDouble(lineArr[5]);
                String Formula = lineArr[6];
                int Carbon = Integer.parseInt(lineArr[7]);
                int Hydrogen = Integer.parseInt(lineArr[8]);
                int Oxygen = Integer.parseInt(lineArr[9]);
                int Nitrogen = Integer.parseInt(lineArr[10]);
                int Phosphorus = Integer.parseInt(lineArr[11]);

                codeToLinearCodeMap.put(Code,new LinearCode(Code, FullName, CommonConfig, 
                                                        Class, ClassSymbol, UnitMolarMass, 
                                                            Formula, Carbon, Hydrogen, 
                                                                Oxygen, Nitrogen, Phosphorus));
            }
        }
        reader.close();
        
    }
    
    
    /**
     * 
     * @param linearCode
     * @return
     */
    public GlycanComposition parseLinearCode(String linearCode) {
        //throw new UnsupportedOperationException("Not yet implemented");
        GlycanComposition composition = null;
        HashMap<String,Integer> glycMonomerTypeToNumberMap = new HashMap<String,Integer>();
        HashMap<String,Integer> elementToNumberMap = new HashMap<String,Integer>();
        int monomers = 0;
       
        // mol wt. H2O=18.01528
        // monomer component ("N","H","F","S","s","x") to number of units
            // N = HexNAc
            // H = Hexose
            // F = Fucose
            // S = Sialic (N-AcetylNeuraminic Acid)
            // s = Neuraminic Acid
            // x = Unaccounted monomer unit 
        /*
         * N = {GN,AN,MN}
         * H = {G,A,M}
         * F = {F}
         * S = {NN}
         * s = {N}
         * x = {Others}
         */
        
        linearCode = linearCode.split(";")[0]; //remove potential amino acid representation at the trailing end....
        char[] lCArray = linearCode.toCharArray();
        for (int i = 0; i < lCArray.length; i++){//char c : lCArray){
            // get code
            String code = null;
            char c = lCArray[i];
            if(c=='G'||c=='A'||c=='M'||c=='N'){                
                if(i < lCArray.length-1){
                    //check the next representation
                    if(lCArray[i+1]=='N'){ //its an HexNAc
                        code = String.valueOf(c)+String.valueOf(lCArray[i+1]);
                        //get code properties from lookup table
                        LinearCode codeObject = codeToLinearCodeMap.get(code);
                        monomers++; // update monomer (monosaccharide) count
                        updateGlycanElementToNumbersMap(monomers,codeObject,elementToNumberMap);                       
                        
                        if(code.equalsIgnoreCase("NN")==false){//is not Sialic acid 'NN', it is a HexNAc(N) - GN,AN,or MN
                            if(glycMonomerTypeToNumberMap.containsKey("N")){
                                int num = glycMonomerTypeToNumberMap.remove("N");
                                glycMonomerTypeToNumberMap.put("N", num+1);
                            }else{
                                glycMonomerTypeToNumberMap.put("N", 1);
                            }
                        }else{//it is a Sialic acid (NN)
                            if(glycMonomerTypeToNumberMap.containsKey("S")){
                                int num = glycMonomerTypeToNumberMap.remove("S");
                                glycMonomerTypeToNumberMap.put("S", num+1);
                            }else{
                                glycMonomerTypeToNumberMap.put("S", 1);
                            }                             
                        }
                        i++;
                        continue;
                    }
                }
                code = String.valueOf(c);
                LinearCode codeObject = codeToLinearCodeMap.get(code);
                monomers++;
                updateGlycanElementToNumbersMap(monomers,codeObject,elementToNumberMap);  
                // update glycan compositions...
                if(code.equalsIgnoreCase("N")==false){//is not Neuraminic acid 's', it is a Hexose(H) - G, A, or M 
                    if(glycMonomerTypeToNumberMap.containsKey("H")){ // is there an 'H' in the map
                        int num = glycMonomerTypeToNumberMap.remove("H"); // if there is, replace/update its mapping
                        glycMonomerTypeToNumberMap.put("H", num+1);
                    }else{ // insert the first 'H' map
                        glycMonomerTypeToNumberMap.put("H", 1);
                    }
                }else{//it is a Neuraminic acid - N
                    if(glycMonomerTypeToNumberMap.containsKey("s")){ // using 's' to represent Neuraminic acid as opposed to 'S' 
                                                                        // which we've used for N-AcetylNeuraminic acid (Sialic acid)         
                        int num = glycMonomerTypeToNumberMap.remove("s");
                        glycMonomerTypeToNumberMap.put("s", num+1);
                    }else{
                        glycMonomerTypeToNumberMap.put("s", 1);
                    }                             
                }                
            } else { // it is some other code NOT (G,A,M or N)
                code = String.valueOf(c);
                if(codeToLinearCodeMap.containsKey(code)){ // it is a valid glycan monomeric unit code
                    monomers++; // update monomers found
                    LinearCode codeObject = codeToLinearCodeMap.get(code); // get code object...
                    updateGlycanElementToNumbersMap(monomers,codeObject,elementToNumberMap); //update elemental composition...                    
                    
                    //check if it is a Fucose
                    if(code.equalsIgnoreCase("F")){ // it is a Fucose...
                        if(glycMonomerTypeToNumberMap.containsKey("F")){ // is there an 'F' in the map
                            int num = glycMonomerTypeToNumberMap.remove("F"); // if there is, replace/update its mapping
                            glycMonomerTypeToNumberMap.put("F", num+1);
                        }else{ // insert the first 'F' map
                            glycMonomerTypeToNumberMap.put("F", 1);
                        }
                    }else{ // it is some other unaccounted, valid monomeric unit code... 
                        if(glycMonomerTypeToNumberMap.containsKey("x")){ // is there an 'x' entry in the map
                            int num = glycMonomerTypeToNumberMap.remove("x"); // if there is, replace/update its mapping
                            glycMonomerTypeToNumberMap.put("x", num+1);
                        }else{ // insert the first 'F' map
                            glycMonomerTypeToNumberMap.put("x", 1);
                        }
                    }                    
                } else { // it a linkage, bracket, etc...
                    //do nothing
                }
            }           
        }
        composition = new GlycanComposition(glycMonomerTypeToNumberMap,elementToNumberMap);        
        return composition;
    }

    private void updateGlycanElementToNumbersMap(int monomers, LinearCode codeObject, HashMap<String, Integer> elementToNumberMap) {
        // update glycan elemental compositions...
        if(monomers==1){//the first encountered/counted monomer...
            // simple put its elements and respective values in the elementToNumberMap
            elementToNumberMap.put("C",codeObject.getCarbon());
            elementToNumberMap.put("H",codeObject.getHydrogen());
            elementToNumberMap.put("O",codeObject.getOxygen());
            elementToNumberMap.put("N",codeObject.getNitrogen());
            elementToNumberMap.put("P",codeObject.getPhosphorus());
        }else{
            // add element compositions and remove water (H2O), for each new glycan monomer read/found...
            // carbon 
            int carbon = elementToNumberMap.remove("C");
            elementToNumberMap.put("C",codeObject.getCarbon()+carbon);
            // hydrogen 
            int hydrogen = elementToNumberMap.remove("H");
            elementToNumberMap.put("H",(codeObject.getHydrogen()+hydrogen)-2);
            // oxygen 
            int oxygen = elementToNumberMap.remove("O");
            elementToNumberMap.put("O",(codeObject.getOxygen()+oxygen)-1);
            // phosphorus 
            int phosphorus = elementToNumberMap.remove("P");
            elementToNumberMap.put("P",codeObject.getPhosphorus()+phosphorus);
            // nitrogen 
            int nitrogen = elementToNumberMap.remove("N");
            elementToNumberMap.put("N",codeObject.getCarbon()+nitrogen);
        }                        
    }

    
}
